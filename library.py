"""Task6
Створіть прототип програми «Бібліотека», де є можливість перегляду та внесення змін
за структурою: автор: твір. Передбачте можливість виведення на екран сортування
за автором та твором.
"""

authors = {
    "William Shakespeare": ["Romeo and Juliet", "Hamlet", ],
    "Agatha Christie": ["The Big Four", "Murder on the Orient Express"]
}


def print_authors(authors_list):
    for author in authors_list.items():
        print(author)


def sort_authors(authors_list):
    sorted_authors = dict.fromkeys(sorted(authors_list.keys()))
    for name, books in authors_list.items():
        sorted_authors[name] = sorted(books)
    return sorted_authors


def add_book():
    while True:
        name = input("Enter the AUTHOR of book, Name and Surname by a space: ").strip()
        if not is_name_valid(name):
            print("Incorrect Input. Enter the Name and Surname by a space, try again")
            continue
        name = get_title_name(name)
        if is_new_author(name):
            print("This author is not in the library, add the author first")
            break
        book = input("Enter the book title: ").strip()
        if not is_new_book(book, name):
            print("This book is is already in the library")
            break
        authors[name].append(book)
        print("The book was added")
        break


def add_author():
    while True:
        name = input("Enter the name and surname of author: ")
        if is_name_valid(name):
            name = get_title_name(name)
            if not is_new_author(name):
                print("This author is already in the library")
                break
            authors[name] = []
            print("Author was added")
            break


def is_name_valid(string):
    name = string.strip().split(" ")
    if len(name) != 2:
        print("Error: Enter NAME AND SURNAME by a space, try again")
        return 0
    if not name[0].isalpha() or not name[1].isalpha():
        print("Error: Use only alphabetic characters to enter Name and Surname, try again")
        return 0
    return True


def is_new_author(name):
    if name in authors:
        return 0
    return True


def is_new_book(title, author_name):
    if title in authors[author_name]:
        return 0
    return True


def get_title_name(string):
    name = string.strip().split(" ")
    name[0] = name[0].title()
    name[1] = name[1].title()
    return " ".join(name)


while True:
    step = input("Enter: \n1 to see authors and their works, \n2 to add a new author or work,"
                 " \n3 to add a book by the author, \n4 to see sorted authors and their works,"
                 "\n5 to exit: ")
    print()

    match step:
        case "1":
            print_authors(authors)
        case "2":
            add_author()
        case "3":
            print_authors(authors)
            add_book()
        case "4":
            print_authors(sort_authors(authors))
        case "5":
            exit()
        case _:
            print("Choose the step 1, 2, 3 or 4")
